﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using analizador_csharp.Utilerias;

namespace analizador_csharp.Datos
{
    class Procesos : ConexionBD
    {
        private const int siguienteProceso = 1;
        private const int grabarProceso = 2;
        private const int grabarRegistrosGenerales = 3;
        private const int grabarRegistrosBaseDeDatos = 4;
        private const int grabarRegistrosEstructura = 5;
        private const int actualizarEstatus = 6;
        private const int recuperarAplicacion = 7;

        private const int estatusInicial = 3;
        private const string datosProcesos = "sp_libProcesos";

        private XmlDocument resultadoXML;
        public XmlDocument ResultadoXML
        {
            get { return resultadoXML; }
        }

        public bool ObtenerProceso()
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(datosProcesos);
                CargaParametro("Tipo", SqlDbType.Int, 8, ParameterDirection.Input, siguienteProceso);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("Procesos");
                    respuesta = xmlNode.HasChildNodes;
                }
                CerrarConexion();
                if (respuesta)
                    EscribeLog("INFO Procesos.ObtenerProceso ejecutado exitosamente.");
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR Procesos.ObtenerProceso " + ex.Message.ToString());
            }
            return respuesta;
        }

        public bool GuardarRegistrosAnalizadoresComunes(int ProcesoID, int AplicacionID, List<ListaRegistros> ListaRegistros)
        {
            bool respuesta = false;
            int analizador = 0;
            try
            {
                bool transaccion = true;
                foreach(var registro in ListaRegistros)
                {
                    PreparaStoredProcedureMasivo(datosProcesos);

                    if (transaccion)
                        InicializaTransaccion();

                    AsignaTransaccion();

                    CargaParametro("Tipo", SqlDbType.Int, 8, ParameterDirection.Input, grabarRegistrosGenerales);
                    CargaParametro("Ruta", SqlDbType.VarChar, 1000, ParameterDirection.Input, registro.Ruta);
                    CargaParametro("NumeroLinea", SqlDbType.Int, 8, ParameterDirection.Input, registro.NoLinea);
                    CargaParametro("LineaCodigo", SqlDbType.VarChar, 1000, ParameterDirection.Input, registro.LineaCodigo);
                    CargaParametro("ExtensionArchivo", SqlDbType.VarChar, 10, ParameterDirection.Input, registro.Extension);
                    CargaParametro("IdProceso", SqlDbType.Int, 8, ParameterDirection.Input, ProcesoID);
                    CargaParametro("IdAplicacion", SqlDbType.Int, 8, ParameterDirection.Input, AplicacionID);
                    CargaParametro("ObjetoPadre", SqlDbType.VarChar, 150, ParameterDirection.Input, registro.ObjetoPadre);
                    CargaParametro("IdTipoAnalizador", SqlDbType.Int, 8, ParameterDirection.Input, registro.Analizador);
                    EjecutaStoredProcedureNonQuery();
                    analizador = registro.Analizador;
                    transaccion = false;
                }

                CommitTransaccion();
                CerrarConexion();
                respuesta = true;
                EscribeLog("INFO Procesos.GuardarRegistrosProcesosGenerales Lista de Registros guardada exitosamente. | Aplicación: " + 
                    AplicacionID + " | Analizador: " + analizador + " | Proceso: " + ProcesoID);
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR Procesos.GuardarRegistrosProcesosGenerales " + ex.Message.ToString());
            }
            return respuesta;
        }

        public bool GuardarRegistrosProcesoBaseDeDatos(int ProcesoID, int AplicacionID, List<ListaRegistros> ListaRegistros)
        {
            bool respuesta = false;
            try
            {
                bool transaccion = true;
                foreach (var registro in ListaRegistros)
                {
                    PreparaStoredProcedureMasivo(datosProcesos);

                    if (transaccion)
                        InicializaTransaccion();

                    AsignaTransaccion();

                    CargaParametro("Tipo", SqlDbType.Int, 8, ParameterDirection.Input, grabarRegistrosBaseDeDatos);
                    CargaParametro("Ruta", SqlDbType.VarChar, 1000, ParameterDirection.Input, registro.Ruta);
                    CargaParametro("NumeroLinea", SqlDbType.Int, 8, ParameterDirection.Input, registro.NoLinea);
                    CargaParametro("LineaCodigo", SqlDbType.VarChar, 1000, ParameterDirection.Input, registro.LineaCodigo);
                    CargaParametro("ExtensionArchivo", SqlDbType.VarChar, 10, ParameterDirection.Input, registro.Extension);
                    CargaParametro("IdProceso", SqlDbType.Int, 8, ParameterDirection.Input, ProcesoID);
                    CargaParametro("IdAplicacion", SqlDbType.Int, 8, ParameterDirection.Input, AplicacionID);
                    CargaParametro("ObjetoPadre", SqlDbType.VarChar, 150, ParameterDirection.Input, registro.ObjetoPadre);
                    CargaParametro("IdObjetoBD", SqlDbType.Int, 8, ParameterDirection.Input, registro.IdObjetoBD);
                    EjecutaStoredProcedureNonQuery();
                    transaccion = false;
                }

                CommitTransaccion();
                CerrarConexion();
                respuesta = true;
                EscribeLog("INFO Procesos.GuardarRegistrosProcesoBaseDeDatos Lista de Registros guardada exitosamente. | Aplicación: " +
                    AplicacionID + " | Analizador: 6 | Proceso: " + ProcesoID);
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR Procesos.GuardarRegistrosProcesoBaseDeDatos " + ex.Message.ToString());
            }
            return respuesta;
        }

        public bool GuardarRegistrosProcesoEstructura(int ProcesoID, int AplicacionID, List<ListaEstructura> ListaRegistros)
        {
            bool respuesta = false;
            try
            {
                bool transaccion = true;
                foreach (var registro in ListaRegistros)
                {
                    PreparaStoredProcedureMasivo(datosProcesos);

                    if (transaccion)
                        InicializaTransaccion();

                    AsignaTransaccion();

                    CargaParametro("Tipo", SqlDbType.Int, 8, ParameterDirection.Input, grabarRegistrosEstructura);
                    CargaParametro("Ruta", SqlDbType.VarChar, 1000, ParameterDirection.Input, registro.Ruta);
                    CargaParametro("NumeroLinea", SqlDbType.Int, 8, ParameterDirection.Input, registro.NoLinea);
                    CargaParametro("LineaCodigo", SqlDbType.VarChar, 1000, ParameterDirection.Input, registro.LineaCodigo);
                    CargaParametro("ExtensionArchivo", SqlDbType.VarChar, 10, ParameterDirection.Input, registro.Extension);
                    CargaParametro("IdProceso", SqlDbType.Int, 8, ParameterDirection.Input, ProcesoID);
                    CargaParametro("IdAplicacion", SqlDbType.Int, 8, ParameterDirection.Input, AplicacionID);
                    CargaParametro("Archivo", SqlDbType.VarChar, 150, ParameterDirection.Input, registro.Archivo);
                    CargaParametro("Nombre", SqlDbType.VarChar, 150, ParameterDirection.Input, registro.Nombre);
                    CargaParametro("TipoElemento", SqlDbType.VarChar, 150, ParameterDirection.Input, registro.Tipo);
                    CargaParametro("Padre", SqlDbType.VarChar, 150, ParameterDirection.Input, registro.Padre);
                    CargaParametro("NombreSecundario", SqlDbType.VarChar, 150, ParameterDirection.Input, registro.NombreSecundario);
                    EjecutaStoredProcedureNonQuery();
                    transaccion = false;
                }

                CommitTransaccion();
                CerrarConexion();
                respuesta = true;
                EscribeLog("INFO Procesos.GuardarRegistrosProcesoEstructura Lista de Registros guardada exitosamente. | Aplicación: " +
                    AplicacionID + " | Analizador: 6 | Proceso: " + ProcesoID);
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR Procesos.GuardarRegistrosProcesoEstructura " + ex.Message.ToString());
            }
            return respuesta;
        }

        public bool GenerarProceso(int ProcesoID, int UsuarioID, int AplicacionID, int AnalizadorID)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(datosProcesos);
                AsignaTransaccion();

                CargaParametro("Tipo", SqlDbType.Int, 8, ParameterDirection.Input, grabarProceso);
                CargaParametro("IdProceso", SqlDbType.Int, 8, ParameterDirection.Input, ProcesoID);
                CargaParametro("IdUsuario", SqlDbType.Int, 8, ParameterDirection.Input, UsuarioID);
                CargaParametro("IdTipoAnalizador", SqlDbType.Int, 8, ParameterDirection.Input, AnalizadorID);
                CargaParametro("IdAplicacion", SqlDbType.Int, 8, ParameterDirection.Input, AplicacionID);
                CargaParametro("ObjetoPadre", SqlDbType.VarChar, 150, ParameterDirection.Input, estatusInicial);
                
                EjecutaStoredProcedureNonQuery();

                CerrarConexion();
                respuesta = true;

                if (respuesta)
                    EscribeLog("INFO Procesos.GenerarProceso Proceso iniciado exitosamente. | Aplicación: " +
                    AplicacionID + " | Analizador: " + AnalizadorID + " | Proceso: " + ProcesoID);
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR Procesos.GuardarRegistrosProcesosGenerales " + ex.Message.ToString());
            }
            return respuesta;
        }

        public bool ActualizaEstatusProceso(int ProcesoID, int AnalizadorID, int EstatusID)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(datosProcesos);
                AsignaTransaccion();

                CargaParametro("Tipo", SqlDbType.Int, 8, ParameterDirection.Input, actualizarEstatus);
                CargaParametro("IdProceso", SqlDbType.Int, 8, ParameterDirection.Input, ProcesoID);
                CargaParametro("IdTipoAnalizador", SqlDbType.Int, 8, ParameterDirection.Input, AnalizadorID);
                CargaParametro("IdEstatus", SqlDbType.Int, 8, ParameterDirection.Input, EstatusID);

                EjecutaStoredProcedureNonQuery();

                CerrarConexion();
                respuesta = true;

                if (respuesta)
                    EscribeLog("INFO Procesos.ActualizaEstatusProceso Proceso iniciado exitosamente. | Analizador: " +
                        AnalizadorID + " | Proceso: " + ProcesoID + " | Estatus: " + EstatusID);
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR Procesos.ActualizaEstatusProceso " + ex.Message.ToString());
            }
            return respuesta;
        }

        public bool RecuperarAplicacion(int AplicacionID)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(datosProcesos);
                CargaParametro("Tipo", SqlDbType.Int, 8, ParameterDirection.Input, recuperarAplicacion);
                CargaParametro("IdAplicacion", SqlDbType.Int, 8, ParameterDirection.Input, AplicacionID);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("Procesos");
                    respuesta = xmlNode.HasChildNodes;
                }
                CerrarConexion();
                if (respuesta)
                    EscribeLog("INFO Procesos.RecuperarAplicacion ejecutado exitosamente.");
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR Procesos.RecuperarAplicacion " + ex.Message.ToString());
            }
            return respuesta;
        }
    }
}
