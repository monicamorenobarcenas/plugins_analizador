﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Data;
using System.Data.SqlClient;

namespace analizador_csharp.Datos
{
    class Archivos : ConexionBD
    {
        private const int archivos = 1;
        private const string obtenerArchivos = "sp_libArchivos-Extensiones";

        private XmlDocument resultadoXML;
        public XmlDocument ResultadoXML
        {
            get { return resultadoXML; }
        }

        public bool ObtenerArchivos(int AplicacionID, int AnalizadorID)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(obtenerArchivos);
                CargaParametro("Tipo", SqlDbType.Int, 8, ParameterDirection.Input, archivos);
                CargaParametro("IdAplicacion", SqlDbType.Int, 8, ParameterDirection.Input, AplicacionID);
                CargaParametro("TipoAnalizador", SqlDbType.Int, 8, ParameterDirection.Input, AnalizadorID);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("Archivos");
                    respuesta = xmlNode.HasChildNodes;
                }
                CerrarConexion();
                if (respuesta)
                    EscribeLog("INFO Archivos.ObtenerArchivos ejecutado exitosamente. | Analizador: " + AnalizadorID);
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR Archivos.ObtenerArchivos " + ex.Message.ToString());
            }
            return respuesta;
        }
    }
}
