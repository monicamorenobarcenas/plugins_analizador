﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using analizador_csharp.Analizadores;
using analizador_csharp.Comunes;
using analizador_csharp.LogFisico;
using analizador_csharp.Utilerias;

namespace analizador_csharp
{
    public class Inicio : Log
    {
        public string Cadxxx;
        CadenaConexion cadenaConexion = new CadenaConexion();

        public Inicio()
        {

        }

        public bool IniciarAnalisis(int UsuarioID, int AplicacionID, int[] Analizadores, string Ruta, string Cadena)
        {
            bool respuesta = false;
            cadenaConexion.Cadena = Cadena;
            try
            {
                // Recuperar el ProcesoID siguiente 
                int nuevoProceso;
                var administrarProcesos = new AdministrarProcesos();

                nuevoProceso = administrarProcesos.ObtenerProceso();

                if (nuevoProceso == 0)
                    throw new Exception();

                // Ejecutar el analizador correspondiente por cada tipo de analizador que es enviado en el arreglo
                foreach (var analizador in Analizadores)
                {
                    switch (analizador)
                    {
                        case 1:
                            var informacionGeneral = new InformacionGeneral();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador))
                            {
                                respuesta = informacionGeneral.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso);
                                EscribeLog("INFO Información General terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 2:
                            var tecnologias = new Tecnologias();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador))
                            {
                                respuesta = tecnologias.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso);
                                EscribeLog("INFO Tecnologías terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 3:
                            var insumos = new Insumos();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador))
                            {
                                respuesta = insumos.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso);
                                EscribeLog("INFO Insumos terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 4:
                            var dependencias = new Dependencias();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador))
                            {
                                respuesta = dependencias.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso);
                                EscribeLog("INFO Dependencias terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 5:
                            var servidores = new Servidores();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador))
                            {
                                respuesta = servidores.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso);
                                EscribeLog("INFO Servidores terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 6:
                            var objetosBaseDeDatos = new ObjetosBaseDeDatos();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador))
                            {
                                respuesta = objetosBaseDeDatos.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso);
                                EscribeLog("INFO Objetos Base de Datos terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 7:
                            var interfaces = new Interfaces();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador))
                            {
                                respuesta = interfaces.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso);
                                EscribeLog("INFO Interfaces terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;

                        case 8:
                            var estructura = new Estructura();
                            if (administrarProcesos.GenerarProceso(nuevoProceso, UsuarioID, AplicacionID, analizador))
                            {
                                respuesta = estructura.Analizar(UsuarioID, AplicacionID, analizador, Ruta, nuevoProceso);
                                EscribeLog("INFO Estructura terminado exitosamente. | Aplicación: " + AplicacionID);
                            }
                            break;
                    }
                }
                
            }

            catch (Exception ex)
            {
                EscribeLog ("ERROR Inicio.IniciarAnalisis " + ex.Message.ToString());
            }

            return respuesta;
        }
    }
}
