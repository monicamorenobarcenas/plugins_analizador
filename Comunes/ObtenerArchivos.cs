﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.IO;
using analizador_csharp.LogFisico;
using analizador_csharp.Datos;

namespace analizador_csharp.Comunes
{
    class ObtenerArchivos : Log
    {
        // Recupera la lista de archivos que se deben analizar de acuerdo a las extensiones establecidas en la base de datos
        public HashSet<string> ListaArchivos(int AplicacionID, int AnalizadorID, string Ruta)
        {
            var listaArchivos = new HashSet<string>();

            try
            {
                var archivos = new Archivos();

                if (archivos.ObtenerArchivos(AplicacionID, AnalizadorID))
                {
                    string[] extensiones;
                    var xmlArchivos = archivos.ResultadoXML;

                    foreach (XmlNode xmlNode in xmlArchivos.DocumentElement.SelectSingleNode("Archivos").SelectNodes("row"))
                    {
                        extensiones = xmlNode.Attributes["Extensiones"].Value.Split('|');

                        foreach (string extension in extensiones)
                        {
                            string[] arregloArchivos = Directory.GetFiles(Ruta, "*." + extension, SearchOption.AllDirectories);
                            for (int i = 0; i <= arregloArchivos.Count() - 1; i++)
                            {
                                listaArchivos.Add(arregloArchivos[i].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR ObtenerArchivos.ListaArchivos " + ex.Message.ToString());
            }
            
            return listaArchivos;
        }
    }
}
