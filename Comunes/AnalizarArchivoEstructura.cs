﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using analizador_csharp.Utilerias;
using analizador_csharp.LogFisico;
using analizador_csharp.Datos;

namespace analizador_csharp.Comunes
{
    class AnalizarArchivoEstructura : Log
    {
        // Recuperar el Inventario de elementos válidos para tener con que comparar las instancias y llamados desde otras clases

        public HashSet<ControlEstructura> ObtenerInventario(int AplicacionID, int AnalizadorID, HashSet<string> ListaArchivos)
        {
            var listaInventario = new HashSet<ControlEstructura>();

            try
            {
                var comentarios = new Comentarios();
                var obtenerEstructura = new ObtenerEstructura();

                // Realizar la búsqueda de patrones en las líneas de código
                foreach (string archivo in ListaArchivos)
                {
                    int noLinea = 0;
                    var fileStream = new FileStream(archivo, FileMode.Open, FileAccess.Read);

                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                    {
                        string lineaCodigo;
                        while ((lineaCodigo = streamReader.ReadLine()) != null)
                        {
                            ++noLinea;

                            if (!string.IsNullOrEmpty(lineaCodigo.Trim()))
                            {
                                if (!comentarios.EsComentario(lineaCodigo.Trim()))
                                {
                                    // Recuperar lista de coincidencias, cada que coincide un patrón con una línea de código para los analizadores comunes
                                    if (obtenerEstructura.ListaEstructura(archivo, noLinea, lineaCodigo, string.Empty, true, listaInventario))
                                    {
                                        if (obtenerEstructura.Tipo != "Librería")
                                            listaInventario.Add(new ControlEstructura(
                                                0, obtenerEstructura.Nombre));
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR AnalizarArchivo.LeerArchivo " + ex.Message.ToString());
            }

            return listaInventario;
        }


        // Recuperar la lista de patrones a buscar, compararlos contra cada línea de código y recuperar la lista de coincidencias,
        public List<ListaEstructura> LeerArchivo(int AplicacionID, int AnalizadorID, HashSet<string> ListaArchivos, 
            HashSet<ControlEstructura> ListaInventario)
        {
            var listaRegistros = new List<ListaEstructura>();
            string ultimoNombreValido = "";

            try
            {
                var comentarios = new Comentarios();
                var obtenerEstructura = new ObtenerEstructura();

                // Comparar los patrones contra las líneas de código
                foreach (string archivo in ListaArchivos)
                {
                    int noLinea = 0;
                    var fileStream = new FileStream(archivo, FileMode.Open, FileAccess.Read);

                    //Generar el registro correspodinete al archivo; este caso sólo aplica para el Analizador Estructura
                    listaRegistros.Add(new ListaEstructura(
                                            archivo,
                                            noLinea,
                                            string.Empty,
                                            Regex.Replace(Path.GetExtension(archivo), @"\.", string.Empty).Trim(),
                                            Regex.Replace(Path.GetFileName(archivo), @"\.\w+", string.Empty).Trim(),
                                            Regex.Replace(Path.GetFileName(archivo), @"\.\w+", string.Empty).Trim(),
                                            "Archivo",
                                            obtenerEstructura.NombreAplicacion(AplicacionID),
                                            string.Empty));

                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                    {
                        string lineaCodigo;
                        while ((lineaCodigo = streamReader.ReadLine()) != null)
                        {
                            ++noLinea;

                            if (!string.IsNullOrEmpty(lineaCodigo.Trim()))
                            {
                                if (!comentarios.EsComentario(lineaCodigo))
                                {
                                    // Recuperar lista de coincidencias, cada que coincide un patrón con una línea de código para los analizadores comunes
                                    if (obtenerEstructura.ListaEstructura(archivo, noLinea, lineaCodigo, ultimoNombreValido, false, ListaInventario))
                                    {
                                        listaRegistros.Add(new ListaEstructura(
                                            archivo,
                                            noLinea,
                                            lineaCodigo,
                                            Regex.Replace(Path.GetExtension(archivo), @"\.", string.Empty).Trim(),
                                            Regex.Replace(Path.GetFileName(archivo), @"\.\w+", string.Empty).Trim(),
                                            obtenerEstructura.Nombre,
                                            obtenerEstructura.Tipo,
                                            obtenerEstructura.Padre,
                                            string.Empty));
                                        ultimoNombreValido = obtenerEstructura.Nombre;
                                    }
                                        
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR AnalizarArchivo.LeerArchivo " + ex.Message.ToString());
            }

            return listaRegistros;
        }
    }
}