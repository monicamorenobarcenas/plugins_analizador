﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using analizador_csharp.Utilerias;
using analizador_csharp.LogFisico;
using analizador_csharp.Datos;

namespace analizador_csharp.Comunes
{
    class AnalizarArchivo : Log
    {
        // Recuperar la lista de patrones a buscar, compararlos contra cada línea de código y recuperar la lista de coincidencias,
        // Para el tipo de analizador "Información General" se deben incluir las líneas comentadas de código
        public List<ListaRegistros> LeerArchivo(int AplicacionID, int AnalizadorID, HashSet<string> ListaArchivos)
        {
            var listaRegistros = new List<ListaRegistros>();
            const int analizadorInformacionGeneral = 1;
            const int IdObjetoVacio = 0;

            try
            {
                var listaPatrones = new List<string>();
                var archivos = new Archivos();
                var compararPatrones = new CompararPatrones();
                var comentarios = new Comentarios();

                // Recuperar lista de patrones, para el Analizador de Objetos de Bases de Datos se obtiene una lista diferente
                if (archivos.ObtenerArchivos(AplicacionID, AnalizadorID))
                {
                    var xmlArchivos = archivos.ResultadoXML;

                    foreach (XmlNode xmlNode in xmlArchivos.DocumentElement.SelectSingleNode("Archivos").SelectNodes("row"))
                        listaPatrones.Add(xmlNode.Attributes["Patron"].Value.ToString());
                }

                // Comparar los patrones contra las líneas de código
                foreach (string archivo in ListaArchivos)
                {
                    int noLinea = 0;
                    var fileStream = new FileStream(archivo, FileMode.Open, FileAccess.Read);

                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                    {
                        string lineaCodigo;
                        while ((lineaCodigo = streamReader.ReadLine()) != null)
                        {
                            ++noLinea;

                            if (!string.IsNullOrEmpty(lineaCodigo.Trim()))
                            {
                                if (!comentarios.EsComentario(lineaCodigo) || AnalizadorID == analizadorInformacionGeneral)
                                {
                                    // Recuperar lista de coincidencias, cada que coincide un patrón con una línea de código para los analizadores comunes
                                    if (listaPatrones.Count > 0)
                                        if (compararPatrones.CompararPatron(lineaCodigo, listaPatrones))
                                            listaRegistros.Add(new ListaRegistros(
                                                archivo,
                                                noLinea,
                                                lineaCodigo,
                                                Regex.Replace(Path.GetExtension(archivo), @"\.", string.Empty).Trim(),
                                                IdObjetoVacio,
                                                Regex.Replace(Path.GetFileName(archivo), @"\.\w+", string.Empty).Trim(),
                                                AnalizadorID));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EscribeLog("ERROR AnalizarArchivo.LeerArchivo " + ex.Message.ToString());
            }

            return listaRegistros;
        }
    }
}