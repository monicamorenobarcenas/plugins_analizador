﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using analizador_csharp.Comunes;
using analizador_csharp.LogFisico;
using analizador_csharp.Utilerias;

namespace analizador_csharp.Analizadores
{
    class Estructura : Log
    {
        public bool Analizar(int UsuarioID, int AplicacionID, int AnalizadorID, string Ruta, int ProcesoID)
        {
            bool respuesta = false;
            int estatusFinalizado = 4;
            int estatusIncompleto = 5;
            var administrarProcesos = new AdministrarProcesos();

            try
            {
                var listaArchivos = new HashSet<string>();
                var listaInventario = new HashSet<ControlEstructura>();
                var listaRegistros = new List<ListaEstructura>();

                var obtenerArchivos = new ObtenerArchivos();
                var analizarArchivoEstructura = new AnalizarArchivoEstructura();
                var grabarRegistros = new GrabarRegistros();

                // 1. Recuperar la lista de archivos que se analizarán
                listaArchivos = obtenerArchivos.ListaArchivos(AplicacionID, AnalizadorID, Ruta);

                EscribeLog("INFO obtenerArchivos.ListaArchivos ejecutado exitosamente. | Aplicación: " + AplicacionID +
                    " | Analizador: " + AnalizadorID + " | Proceso: " + ProcesoID);

                //2. Leer cada archivo de la lista, generar un inventario de elementos para buscar las instancias 
                if (listaArchivos.Count > 0)
                    listaInventario = analizarArchivoEstructura.ObtenerInventario(AplicacionID, AnalizadorID, listaArchivos);

                EscribeLog("INFO analizarArchivo.LeerArchivo (Generar Inventario) ejecutado exitosamente. | Aplicación: " + AplicacionID +
                    " | Analizador: " + AnalizadorID + " | Proceso: " + ProcesoID);

                //3. Leer cada archivo de la lista, comparar cada línea de código con los elementos particulares del lenguaje
                // Recuperar la lista de coincidencias

                if (listaInventario.Count > 0)
                    listaRegistros = analizarArchivoEstructura.LeerArchivo(AplicacionID, AnalizadorID, listaArchivos, listaInventario);

                EscribeLog("INFO analizarArchivo.LeerArchivo ejecutado exitosamente. | Aplicación: " + AplicacionID +
                    " | Analizador: " + AnalizadorID + " | Proceso: " + ProcesoID);


                //4. Insertar la lista de coincidencias en la base de datos
                if (listaRegistros.Count > 0)
                    respuesta = grabarRegistros.GuardarListaRegistros(ProcesoID, AplicacionID, AnalizadorID, listaRegistros);

                EscribeLog("INFO grabarRegistros.GuardarListaRegistros ejecutado exitosamente. | Aplicación: " + AplicacionID +
                    " | Analizador: " + AnalizadorID + " | Proceso: " + ProcesoID);

                //5. Cerrar el proceso con estatus finalizado
                if (respuesta)
                    administrarProcesos.ActualizaProceso(ProcesoID, AnalizadorID, estatusFinalizado);

                EscribeLog("INFO grabarRegistros.ActualizaProceso ejecutado exitosamente. | Aplicación: " + AplicacionID +
                    " | Analizador: " + AnalizadorID + " | Proceso: " + ProcesoID + " | Estatus: " + estatusFinalizado);
            }
            catch (Exception ex)
            {
                administrarProcesos.ActualizaProceso(ProcesoID, AnalizadorID, estatusIncompleto);
                EscribeLog("ERROR Estructura.Analizar " + ex.Message.ToString());
            }

            return respuesta;
        }
    }
}
