﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using analizador_csharp.Comunes;
using analizador_csharp.Utilerias;
using analizador_csharp.LogFisico;

namespace analizador_csharp.Analizadores
{
    class Tecnologias : Log
    {
        public bool Analizar(int UsuarioID, int AplicacionID, int AnalizadorID, string Ruta, int ProcesoID)
        {
            bool respuesta = false;
            int estatusFinalizado = 4;
            int estatusIncompleto = 5;
            var administrarProcesos = new AdministrarProcesos();

            try
            {
                var listaArchivos = new HashSet<string>();
                var listaRegistros = new List<ListaRegistros>();

                var obtenerArchivos = new ObtenerArchivos();
                var analizarArchivo = new AnalizarArchivo();
                var grabarRegistros = new GrabarRegistros();

                // 1. Recuperar la lista de archivos que se analizarán
                listaArchivos = obtenerArchivos.ListaArchivos(AplicacionID, AnalizadorID, Ruta);

                EscribeLog("INFO obtenerArchivos.ListaArchivos ejecutado exitosamente. | Aplicación: " + AplicacionID +
                    " | Analizador: " + AnalizadorID + " | Proceso: " + ProcesoID);

                //2. Leer cada archivo de la lista, comparar cada línea de código con los patrones y recuperar la lista de coincidencias
                if (listaArchivos.Count > 0)
                    listaRegistros = analizarArchivo.LeerArchivo(AplicacionID, AnalizadorID, listaArchivos);

                EscribeLog("INFO analizarArchivo.LeerArchivo ejecutado exitosamente. | Aplicación: " + AplicacionID +
                    " | Analizador: " + AnalizadorID + " | Proceso: " + ProcesoID);

                //3. Insertar la lista de coincidencias en la base de datos
                if (listaRegistros.Count > 0)
                    respuesta = grabarRegistros.GuardarListaRegistros(ProcesoID, AplicacionID, AnalizadorID, listaRegistros);

                EscribeLog("INFO grabarRegistros.GuardarListaRegistros ejecutado exitosamente. | Aplicación: " + AplicacionID +
                    " | Analizador: " + AnalizadorID + " | Proceso: " + ProcesoID);

                //4. Cerrar el proceso con estatus finalizado
                if (respuesta)
                    administrarProcesos.ActualizaProceso(ProcesoID, AnalizadorID, estatusFinalizado);

                EscribeLog("INFO grabarRegistros.ActualizaProceso ejecutado exitosamente. | Aplicación: " + AplicacionID +
                    " | Analizador: " + AnalizadorID + " | Proceso: " + ProcesoID + " | Estatus: " + estatusFinalizado);
            }
            catch (Exception ex)
            {
                administrarProcesos.ActualizaProceso(ProcesoID, AnalizadorID, estatusIncompleto);
                EscribeLog("ERROR Tecnologias.Analizar " + ex.Message.ToString());
            }

            return respuesta;
        }
    }
}
